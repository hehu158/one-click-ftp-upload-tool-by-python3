# python3一键ftp上传工具

#### 介绍
vue代码上传比较麻烦，特别是assets文件夹，每次打包生成的js跟css文件都不一样，手动去删然后再上传又比较麻烦了，特意用py写了一个ftp上传工具，上传前会事先清空远程服务器文件夹路径下的所有文件及文件夹，然后再上传本地代码。

#### 软件架构
软件架构说明


#### 安装教程&&使用说明

1.  系统下安装python3
2.  配置好ftp，默认dist为待上传的代码，服务器端文件夹路径为/dist

![服务器端根目录下需要dist文件夹路径](https://images.gitee.com/uploads/images/2021/0812/081310_75464ddd_568397.png "服务器端根目录下需要dist文件夹路径")

服务器端根目录下需要dist文件夹路径

![本地py跟dist文件夹路径存放位置说明](https://images.gitee.com/uploads/images/2021/0812/081235_ef3dd4ed_568397.png "本地py跟dist文件夹路径存放位置说明")

本地py跟dist文件夹路径存放位置说明

3.  命令窗体下执行python fileUpload.py即可
